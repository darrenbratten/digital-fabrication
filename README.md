
# My Bio

## Introduction :smiley:

Hi! My name is Darren Bratten and I’m a second year MA student from the *Collaborative & Industrial Design* course at Aalto University, Finland. 

What you might like to know about me: 
- I'm from Ireland and I moved to Finland in 2019 
- I’ve a BSc degree in *Product Design*, where I studied Engineering, Marketing, and Design 
- I’ve spent roughly five years working in Design/Manufacturing services making event props, packaging, and interior furniture
- In my work I’m inspired to create unique user experiences and high-quality, usable designs 
- In my personal life I’m enthusiastic about music and playing musical instruments 

## Fab Academy 2022

### Why am I taking Fab Academy? :question:

I undoubtedly enjoy hands-on design activities but creating prototypes and producing artefacts is an important aspect of my research too. In order to pursue *Product* & *Industrial* Design further, I need to be able to realise my ideas so that I can evaluate them. By learning digital fabrication and electronics production, I hope to be able to anticipate how viable and feasible design concepts may be. It should also improve the efficiency of my projects and open up opportunities to be creative with technology. 

> In order to pursue *Product* & *Industrial* Design further, I need to be able to realise my ideas so that I can evaluate them.

Thus far in my career, I’ve worked closely with softwood, hardwood, wood panels, steel, paper, card, cardboard, and foamcore for example. About one fifth of that experience has included using digital fabrication whilst the rest has been using handwork. But as we enter into this third digital revolution, it seems like an absolute necessity to take advantage of such access to Fab Lab technologies. Not only does it democratise innovation for many parties and enable personalisation, but it will allow a designer, such as myself, to gain some experience with technologies which were previously monopolised by corporations e.g. CNC milling, 3d printing, laser cutting, and more. Indeed, it’s a privilege to have the opportunity. 

Overall I expect that I’ll be a better Designer and Engineer at the end of it. I may even have an interest to extend my work by joining the official Fab Academy. At this stage in my professional life, I realise that being able to prototype and produce design is critical for my work. The same is true for having the opportunity to learn from the growing Fab Lab community and also share my own work for peer review. Either way, I’m glad that I will have the chance to engage in practice-orientated research and learn about the benefits of an open source network. 

### What I plan to do in Fab Academy 2022 :clipboard:

As part of the course and the final project I hope to reach several objectives. 

I want to: 
- [ ] Explore various machines and push the boundaries of material & construction 
- [ ] Produce a relatively cohesive set of artefacts which coalesce into a final project 
- [ ] Document my research in a visual and meaningful way that describes my design-thinking

### My final project proposal 

Music has played a large part in my life. Go with me on this. In my teenage years I remember ~~burning~~ buying CDs of my favourite artists. Like many, my taste had developed from early exposure to *pop* music, but coarsely followed with a rebellion of *soft rock*, *hard rock*, *grunge*, and *metal*. This later continued with an interest in *folk*, *blues*, *jazz*, and *classical*. Since I was 16 I’ve been playing guitar and more recently I’ve been learning the violin. I’ve also thrown my hand at harmonica, bongos, and keyboard. In 2021 I became captivated with synthesisers. There is something about music which I believe shapes a person’s identity, or at the very least, keeps us entertained. 

> In 2021 I became captivated with synthesisers. 

In this digital era, music production is easily much more accessible with internet technologies and free computer software. Analog (including electronic) instruments are also incredibly affordable, for the most part. Of course learning a musical instrument is invariably a black box for some people. And trust me, I know from experience, it generally takes time and practice to reap the rewards. Yet, I have a suspicion that most people would like to play an instrument, or to be part of the process somehow. Indeed, according to this [survey](https://blog.jakpat.net/peoples-interest-in-playing-a-musical-instrument-survey-report/), 80% of circa 600 respondents reported an interest in playing a musical instrument. Possibly, this is enough grounds to validate my investigation. Therefore, my preliminary and overarching research question will be “what can I design to make music creation or participation more accessible?”.  

> 80% of circa 600 respondents reported an interest in playing a musical instrument

>> **what can I design to make music creation or participation more accessible?**

This criteria should leave me enough room to explore the concept and is purposefully left *ill-structured* to start. In this way I can gradually introduce more constraints as my interest in a problem and solution becomes more apparent. In fact, I may benefit and accelerate this process if I partake in some user research. Of course, from my ‘semi-expert’ experience of playing music, I might overlook certain principles regarding ease-of-use and intuition, unless I draw extensively from principles of psychology and affordances, for example. Secondly, I could consider some small co-creation exercises to assess the latent needs of non-musicians, since the aspirations of a regular musician requires discipline. At least I will need to evaluate my final project with this target group, concerning the usability and experience. 

# Contact details 

## How to stay in touch 

:email: **Send me an email:**.    
darren.bratten(at)aalto.fi

:link: **Connect with me on Linkedin:**.  
https://www.linkedin.com/in/dbratten/






